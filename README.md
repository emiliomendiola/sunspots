# Sunspots API

This API provides services to access and perform analysis of data on sunspot activity from 1700 to 2014.

Check out our [documentation](docs/user.md) to get started!

### Authors
* Cesar Javier Alvarado
* Emilio A. Mendiola