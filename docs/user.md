# Sunspots API User Documentation

This API is a cloud-based data access service that allows access to and analysis of data on the occurence of sunspots on our Sun from 1700 to 2014.

__Note__: These instruction deploy the service to one machine. To deploy as a distributed service, see [these instructions.](deployment.md)

### Tech

The sunspots API uses a number of open source projects to work properly:

* Redis - database creation tool
* Flask - web-framework development tool
* Docker - containerization software

### Services

Our API currently provides the following services.

| Service | Endpoint |
| ------ | ------ |
| say hello! | Get /hello |
| complete data set | GET /sunspots |
| data from single year | GET /sunspots/year/<value> |
| data from year with correspodning id | GET /sunspots/id/<id> |
| data from range of years | GET /sunspots?start=<year>&end=<year> |
| add a data point | POST /sunspots |
| list all jobs in queue | GET /jobs |
| submit a job | POST /jobs |
| delete a job in the queue | DELETE /jobs/<job id> |
| get the plot from a correspoding job | GET /jobs/<job id>/plot |
| get mean of data from range of years | GET /jobs/<job id>/mean |
| get standard deviation of data from range of years | GET /jobs/<job id>/stdev |
| get analysis (max, min, mean, st. dev.) of data from range of years | GET /jobs/<job id>/analysis |

### Installing the API

The sunspots API is very easy to install and deploy in a Docker container. You need to make sure you have [installed Docker] and [Docker Compose].

Clone the sunspots repository:
```
$ git clone https://emiliomendiola@bitbucket.org/emiliomendiola/sunspots.git
```

Start up the service using the docker-compose file:
```
$ cd sunspots
$ docker-compose up
```

Verify the deployment by making a request to API:
```
$ curl localhost:5000/hello
```


* These instruction deploy the service to one machine. To deploy as a distributed service, see [these instructions.](deployment.md)

### Teardown

Shutdown the service by entering Control-C. It's important to clean up left over containers after you have shutdown the service.
```
$ docker-compose rm
```

### Building the image manually

To build the image locally, navigate to the repository directory.

```
$ docker build -t sunspots-api .
```

Make changes in the docker-compose file to utilize the newly built image: change image from "emiliomendiola/sunspots-api:latest" to "sunspots-api".


### Usage

Open your favorite Terminal and run these commands.

To list data from a range of years:

```
curl localhost:5000/sunspots?start=1800\&end=1805
```  
	  
To add a job to the queue:

```
curl -X POST --header "Content-Type: application/json" --data '{"start":"1970", "end":"1990"}' localhost:5000/jobs
```

  
To get the resulting plot from a completed job:

```
curl localhost:5000/jobs/<job id>/plot
```
  

To get the mean from a range of years, first submit a job with the desired start and end year. Then run:

```
curl localhost:5000/jobs/<job id>/mean
```
To get the standard deviation from a range of years, first submit a job with the desired start and end year. Then run:

```
curl localhost:5000/jobs/<job id>/stdev
```
To get the an analysis from a range of years, first submit a job with the desired start and end year. Then run:

```
curl localhost:5000/jobs/<job id>/analysis
```

To delete a job in the queue:

```
curl -X DELETE localhost:5000/jobs/<job id>
```

### Troubleshooting

If you have problems starting the service you may have old containers from previous runs that were not properly stopped or disposed of. Try cleaning up your docker environment.
```
$ docker stop $(docker ps -aq)
$ docker rmi -f emiliomendiola/sunspots-api:latest
$ docker system prune
```
Answer yes to any prompts. Then try starting the service again.

### Adding/Removing Data Points

It is quite easy to add or remove data that can be accessed by the API. In fact, you could switch out the entire sunspots dataset and replace it with another time series dataset, so long as the layout is the same as in our sunspots.csv file. 
To do so, clone the repo and make the desired modification to sunspots.csv. The name of the CSV file must remain the same as that is how it is referenced in the code. Once the necessary changes are made, start up the service and add the modified file as an external volume.

```
docker-compose up
```

The updated CSV file will be mounted to the container.

#### To do

- migrate over to python alpine images to reduce image size
- add job types

Thanks for using our API! 🌞 

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [installed Docker]: <https://docs.docker.com/install/>
   [Docker Compose]: <https://docs.docker.com/compose/install/>
