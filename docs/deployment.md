# Deployment to Multiple Machines

Deploying the API to multiple machines if fairly easy using Docker's swarm capabilities. The docker-compose-distributed.yml file is used to deploy the Flask API to one node, and the Redis database and worker functions to a different node. Note that the API and Redis database will both run seperate nodes (machines) while the load of the worker service will be distributed across all the worker nodes in the swarm.

For more information on Docker's swarm capabilites see the [Docker swarm documentation].

### Dependencies

To deploy to multiple machines make sure you have [installed Docker] and [Docker Compose].

### Setup

Begin by logging in to a machine. This is the machine that will manage the swarm and where the Flask API will run. Clone the [sunspots Bitbucket repository].

Navigate to the repo and initiate the Docker swarm.

```
$ cd sunspots
$ docker swarm init
```

You can add other machines, called worker nodes, to the swarm by copying the command that appears and running it on the node machines. __Note that only the swarm manager (the machine where you initiated the swarm) can run Docker commands__.

To view the nodes that are part of your swarm, run:

```
$ docker node ls
```

Start up the sunspots service:
```
$ docker stack deploy -c docker-compose-distributed.yml sunspots-service
```

You can view the status of each node by running:
```
$ docker node ps NODE_NAME
```

You should find the API running on the manager node and the Redis database and worker.py running on the worker node(s).

### Teardown

When you have finished, you can remove the service.
```
$ docker service rm sunspots-service_api
$ docker service rm sunspots-service_redis
$ docker service rm sunspots-service_worker
```

Run ``` docker service ls``` to verify that the service has been shutdown.

Remove all nodes from the swarm. You must do by logging in to each node and running:
```
$ docker swarm leave
```

To completley shutdown the swarm, you must have removed all the worker nodes and then force the removal of the managing node. Run the following form the manager node:
```
$ docker swarm leave --force
```

Thanks for using our API! 🌞 


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [sunspots Bitbucket repository]: <https://github.com/emiliomendiola/sunspots
   [Docker swarm documentation]: <https://docs.docker.com/engine/swarm/>
   [installed Docker]: <https://docs.docker.com/install/>
   [Docker Compose]: <https://docs.docker.com/compose/install/>

