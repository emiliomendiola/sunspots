#!/usr/bin/env python3

import requests

# test root collection status code
def test_status_code():
    rsp = requests.get('http://localhost:5000/sunspots')
    assert rsp.status_code == 200

# test root collection returns json
def test_root_json():
    rsp = requests.get('http://localhost:5000/sunspots')
    assert rsp.json()

# test root collection decoded to list
def test_root_is_list():
    rsp = requests.get('http://localhost:5000/sunspots')
    assert type(rsp.json()) == list

# test each element of root collection is dictionary
def test_root_list_is_dictionaries():
    rsp = requests.get('http://localhost:5000/sunspots')
    data = rsp.json()
    for item in data:
        assert type(item) == dict

# test root dictionaries have 3 keys
def test_root_dictionaries_3_keys():
    rsp = requests.get('http://localhost:5000/sunspots')
    data = rsp.json()
    for item in data:
        assert len(item.keys()) == 3

# test root collection key values correct
def test_root_keys_correct():
    rsp = requests.get('http://localhost:5000/sunspots')
    data = rsp.json()
    for item in data:
        keys = list(item.keys())
        assert keys[0] == 'id'
        assert keys[1] == 'sunspots'
        assert keys[2] == 'year'

# test root returns correct number of dictionaries
def test_root_returns_100_dicts():
    rsp = requests.get('http://localhost:5000/sunspots')
    data = rsp.json()
    assert len(data) == 315

# test first and last values from root are correct
def test_root_first_last_correct():
    rsp = requests.get('http://localhost:5000/sunspots')
    data = rsp.json()
    
    # first data point
    assert data[0]['id'] == 0
    assert data[0]['year'] == 1700
    assert data[0]['sunspots'] == 5

    # second data point
    assert data[-1]['id'] == 314
    assert data[-1]['year'] == 2014
    assert data[-1]['sunspots'] == 79

# test status code with start and end parameters
def test_status_given_start_end():
    rsp = requests.get('http://localhost:5000/sunspots?start=1800&end=1805')
    assert rsp.status_code == 200

# test returns json given start and end parameters
def test_returns_json_given_start_end():
    rsp = requests.get('http://localhost:5000/sunspots?start=1800&end=1805')
    assert rsp.json()

# test json can be decoded to list given start and end
def test_list_given_start_end():
    rsp = requests.get('http://localhost:5000/sunspots?start=1800&end=1805')
    assert type(rsp.json()) == list

# test returning correct number of data points given start and end
def test_correct_results_given_start_end():
    rsp = requests.get('http://localhost:5000/sunspots?start=1800&end=1805')
    assert len(rsp.json()) == 6

# test status code given start
def test_status_given_start():
    rsp = requests.get('http://localhost:5000/sunspots?start=1800')
    assert rsp.status_code == 200

# test returns json given start parameters                                                                                                           
def test_returns_json_given_start():
    rsp = requests.get('http://localhost:5000/sunspots?start=1800')
    assert rsp.json()

# test json can be decoded to list given start                                                                                                     
def test_list_given_start():
    rsp = requests.get('http://localhost:5000/sunspots?start=1800')
    assert type(rsp.json()) == list

# test returning correct number of data points given start and end                                                                                           
def test_correct_results_given_start():
    rsp = requests.get('http://localhost:5000/sunspots?start=1800')
    assert len(rsp.json()) == 215

# test status code given end
def test_status_given_end():
    rsp = requests.get('http://localhost:5000/sunspots?end=1800')
    assert rsp.status_code == 200

# test returns json given end parameter                                                      
def test_returns_json_given_end():
    rsp = requests.get('http://localhost:5000/sunspots?end=1800')
    assert rsp.json()

# test json can be decoded to list given end
def test_list_given_end():
    rsp = requests.get('http://localhost:5000/sunspots?end=1800')
    assert type(rsp.json()) == list

# test returning correct number of data points given end
def test_correct_results_given_end():
    rsp = requests.get('http://localhost:5000/sunspots?end=1800')
    assert len(rsp.json()) == 101

# test status code with start and end parameters where start > end
def test_status_given_start_greater_end():
    rsp = requests.get('http://localhost:5000/sunspots?start=1805&end=1800')
    assert rsp.status_code == 400

# test proper error message returned when start > end
def test_list_given_start_greater_end():
    rsp = requests.get('http://localhost:5000/sunspots?start=1805&end=1800')
    data = rsp.json()
    assert 'Error' in data['message']


# test status given limit and offset
def test_status_given_offset_limit():
    rsp = requests.get('http://localhost:5000/sunspots?limit=5&offset=10')
    assert rsp.status_code == 200

# test returns json given limit and offset
def test_returns_json_given_limit_offset():
    rsp = requests.get('http://localhost:5000/sunspots?limit=5&offset=10')
    assert rsp.json()

# test returns list when given limi and offset
def test_returns_list_given_limit_offset():
    rsp = requests.get('http://localhost:5000/sunspots?limit=5&offset=10')
    assert type(rsp.json()) == list

# test number of datapoints returned correct given limit and offset
def test_number_data_returned_correct_given_limit_offset():
    rsp = requests.get('http://localhost:5000/sunspots?limit=5&offset=10')
    assert len(rsp.json()) == 5

# test status given limit
def test_status_given_limit():
    rsp = requests.get('http://localhost:5000/sunspots?limit=5')
    assert rsp.status_code == 200

# test returns json given limit
def test_returns_json_given_limit():
    rsp = requests.get('http://localhost:5000/sunspots?limit=5')
    assert rsp.json()

# test returns list when given limit
def test_returns_list_given_limit():
    rsp = requests.get('http://localhost:5000/sunspots?limit=5')
    assert type(rsp.json()) == list

# test number of datapoints returned correct given limit
def test_number_data_returned_correct_given_limit():
    rsp = requests.get('http://localhost:5000/sunspots?limit=5')
    assert len(rsp.json()) == 5

# test status given offset
def test_status_given_offset():
    rsp = requests.get('http://localhost:5000/sunspots?offset=80')
    assert rsp.status_code == 200

# test returns json given offset                                         
def test_returns_json_given_offset():
    rsp = requests.get('http://localhost:5000/sunspots?offset=80')
    assert rsp.json()

# test returns list when given offset                      
def test_returns_list_given_offset():
    rsp = requests.get('http://localhost:5000/sunspots?offset=80')
    assert type(rsp.json()) == list

# test number of datapoints returned correct given offset                                                                              
def test_number_data_returned_correct_given_offset():
    rsp = requests.get('http://localhost:5000/sunspots?offset=80')
    assert len(rsp.json()) == 235

# test response status when given invalid input start=abc
def test_status_given_invalid_start():
    rsp = requests.get('http://localhost:5000/sunspots?start=abc')
    assert rsp.status_code == 400

# test returns json when give invalid start
def test_returns_json_invalid_start():
    rsp = requests.get('http://localhost:5000/sunspots?start=abc')
    assert rsp.json()

# test returns error message given invalid start
def test_returns_error_invalid_start():
    rsp = requests.get('http://localhost:5000/sunspots?start=abc')
    data = rsp.json()
    assert "Error" in data['message']

# test response status when given invalid input end=abc
def test_status_given_invalid_end():
    rsp = requests.get('http://localhost:5000/sunspots?end=abc')
    assert rsp.status_code == 400

# test returns json when give invalid end
def test_returns_json_invalid_end():
    rsp = requests.get('http://localhost:5000/sunspots?end=abc')
    assert rsp.json()

# test returns error message given invalid end
def test_returns_error_invalid_end():
    rsp = requests.get('http://localhost:5000/sunspots?end=abc')
    data = rsp.json()
    assert "Error" in data['message']
    
# test response status when given invalid limit
def test_status_given_invalid_limit():
    rsp = requests.get('http://localhost:5000/sunspots?limit=abc')
    assert rsp.status_code == 400

# test returns json when given invalid limit
def test_returns_json_invalid_limit():
    rsp = requests.get('http://localhost:5000/sunspots?limit=abc')
    assert rsp.json()

# test returns error message given invalid limit
def test_returns_error_invalid_limit():
    rsp = requests.get('http://localhost:5000/sunspots?limit=abc')
    data = rsp.json()
    assert "Error" in data['message']

# test response status when given invalid offset
def test_status_given_invalid_offset():
    rsp = requests.get('http://localhost:5000/sunspots?offset=abc')
    assert rsp.status_code == 400

# test returns json when give invalid offset
def test_returns_json_invalid_offset():
    rsp = requests.get('http://localhost:5000/sunspots?offset=abc')
    assert rsp.json()

# test returns error message given invalid offset
def test_returns_error_invalid_offset():
    rsp = requests.get('http://localhost:5000/sunspots?start=offset')
    data = rsp.json()
    assert "Error" in data['message']
    
# test response status when given both start/end and offset/limit
def test_status_given_both_start_offset():
    rsp = requests.get('http://localhost:5000/sunspots?start=5&limit=12')
    assert rsp.status_code == 400

# test returns json when given both start/end and limit/offset
def test_returns_json_both_start_offset():
    rsp = requests.get('http://localhost:5000/sunspots?start=5&limit=12')
    assert rsp.json()

# test returns error message given both start/end and limit/offset
def test_returns_error_both_start_offset():
    rsp = requests.get('http://localhost:5000/sunspots?start=5&limit=12')
    data = rsp.json()
    assert "Error" in data['message']
    
# test id endpoint returns status 200
def test_id_status_code():
    rsp = requests.get('http://localhost:5000/sunspots/10')
    assert rsp.status_code == 200

# test id endpoint returns json
def test_id_returns_json():
    rsp = requests.get('http://localhost:5000/sunspots/10')
    assert rsp.json()

# test id returns python dict
def test_id_returns_list():
    rsp = requests.get('http://localhost:5000/sunspots/10')
    assert type(rsp.json()) == dict

# test id returns dict with 3 keys
def test_id_dict_has_3_keys():
    rsp = requests.get('http://localhost:5000/sunspots/10')
    assert len(rsp.json().keys()) == 3

# test id returns correct value
def test_id_returns_correct_value():
    rsp = requests.get('http://localhost:5000/sunspots/10')
    data = rsp.json()
    assert data['id'] == 10

# test year returns valid status code
def test_year_returns_valid_status_code():
    rsp = requests.get('http://localhost:5000/sunspots/year/1800')
    assert rsp.status_code == 200

# test year returns valid json string
def test_year_returns_json():
    rsp = requests.get('http://localhost:5000/sunspots/year/1800')
    assert rsp.json()

# test year response returns dict
def test_year_returns_dict():
    rsp = requests.get('http://localhost:5000/sunspots/year/1800')
    data = rsp.json()
    assert type(data[0]) == dict

# test year returns dict with 3 keys
def test_year_has_3_keys():
    rsp = requests.get('http://localhost:5000/sunspots/year/1800')
    data = rsp.json()
    assert len(data[0].keys()) == 3

# test year returns correct value
def test_year_returns_correct_value():
    rsp = requests.get('http://localhost:5000/sunspots/year/1800')
    data = rsp.json()
    assert data[0]['year'] == 1800
