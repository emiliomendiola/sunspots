from hotqueue import HotQueue
import uuid, redis, time, os, io, math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from dataAccess.dataAccessFunctions import listData

# -------------- Redis IP & Ports --------------
REDIS_IP = os.environ.get('REDIS_IP', '172.17.0.1')
try:
    REDIS_PORT = int(os.environ.get('REDIS_PORT'))
except:
    REDIS_PORT = 6379

# ------------- Redis DBs -------------
DATA_DB = 0
QUEUE_DB = 1

rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=DATA_DB)
q = HotQueue("queue", host=REDIS_IP, port=REDIS_PORT, db=QUEUE_DB)

# ---------- Job Status ----------------
SUBMITTED_STATUS = 'submitted'
IN_PROGRESS = 'in_progress'
COMPLETE_STATUS = 'complete'


# ---------- Private functions ----------
def _generate_jid():
    return str(uuid.uuid4())

def _generate_job_key(jid):
    return 'job.{}'.format(jid)

def _get_time():
    return time.asctime(time.gmtime())+' UTC'

def _instantiate_job(jid, status, create_time, last_update, start, end, plot):
    ''' Creates a python object representing a job '''
    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'start': start,
                'end': end,
                'create_time':create_time,
                'last_update': last_update
        }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'start': start.decode('utf-8'),
            'end': end.decode('utf-8'),
            'create_time': create_time.decode('utf-8'),
            'last_update': last_update.decode('utf-8')
    }

def _save_job(job_key, job_dict):
    """Save a job object in the Redis database."""
    rd.hmset(job_key,job_dict)

def _get_job_by_jobkey(job_key):
    jid, status, start, end, create_time, last_update, plot = rd.hmget(job_key, 'id', 'status', \
                                                    'start', 'end', 'create_time', 'last_update', 'plot')
    if jid:
        return _instantiate_job(jid, status, create_time, last_update, start, end, plot)
    return None



# ----------- Public functions -----------

def add_job(start=None, end=None, status=SUBMITTED_STATUS):
    """Add a job to the redis queue."""
    jid, current_time, last_update = _generate_jid(), _get_time(), _get_time()
    job_dict = _instantiate_job(jid, status, current_time, last_update, start, end, 'None')
    job_key = _generate_job_key(jid)
    _save_job(job_key, job_dict)
    queue_job(jid)
    print("Job has been queued.")
    return job_dict

def update_job_status(jid, status):
    """Update the status of job with job id `jid` to status `status`."""
    job = get_job_by_jid(jid)
    if job:
        job['status'] = status
        job['last_update'] = _get_time()
        _save_job(_generate_job_key(jid), job)
    else:
        raise Exception()

def get_job_by_jid(jid):
    ''' returns job "jid" from redis db '''
    return _get_job_by_jobkey(_generate_job_key(jid))

def get_all_jobs():
    ''' returns a list of all jobs in the Redis db '''
    all_jobs = []
    for job_key in rd.keys():
        all_jobs.append(_get_job_by_jobkey(job_key))
    return all_jobs

def get_job_plot(jid):
    ''' Returns plot, as binary data, from the associated job. '''
    job = get_job_by_jid(jid)
    if job['status'] != COMPLETE_STATUS:
        return "Job not complete."
    else:
        return rd.hmget(_generate_job_key(jid), 'plot')

def get_job_mean(jid):
    '''Returns the average of the data points.'''
    job = get_job_by_jid(jid)
    points = listData(int(job['start']), int(job['end']))
    sunspots = [p['sunspots'] for p in points]
    mean = round(float(sum(sunspots))/len(sunspots), 2)
    return mean

def get_job_std(jid):
    '''Returns the standard deviation of the data points.'''
    job = get_job_by_jid(jid)
    points = listData(int(job['start']), int(job['end']))
    sunspots = [p['sunspots'] for p in points]
    mean = get_job_mean(jid)
    n = len(points)
    top = 0
    for i in range(n):
        top += (sunspots[i] - mean)**2
    std = math.sqrt(top/n)
    return round(std, 2)

def get_job_analysis(jid):
    '''Returns several characteristics of the requested data'''
    '''including max, min, mean, std. dev., variance.'''
    job = get_job_by_jid(jid)
    points = listData(int(job['start']), int(job['end']))
    sunspots = [p['sunspots'] for p in points]
    maxi = max(sunspots)
    mini = min(sunspots)
    stdev = get_job_std(jid)
    mean = get_job_mean(jid)
    return maxi, mini, mean, stdev

def delete_by_jid(jid):
    rd.delete(_generate_job_key(jid))

# ------------ Queue functions -----------

def queue_job(jid):
    ''' Add a job to the redis queue. '''
    q.put(jid)

def finish_job(jid, file_path):
    ''' Update job status in db and plot once job is completed. '''
    job = get_job_by_jid(jid)
    job['plot'] = open(file_path, 'rb').read()
    job['status'] = COMPLETE_STATUS
    rd.hmset(_generate_job_key(jid), job)
    
# ---------- Execution of Job -----------

def execute_job(jid):
    ''' Execute the job. '''
    job = get_job_by_jid(jid)
    points = listData(int(job['start']), int(job['end']))
    years = [int(p['year']) for p in points]
    sunspots = [p['sunspots'] for p in points]
    plt.scatter(years, sunspots)
    plt.title('Observed Sunspots')
    plt.xlabel('Year')
    plt.ylabel('Sunspots')
    tmp_file = '/tmp/{}.png'.format(jid)
    plt.savefig(tmp_file, dpi=150)
    finish_job(jid, tmp_file)
