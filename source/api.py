from flask import Flask, jsonify, request, send_file
import sys, io, jobs
sys.path.append('/sunspots-api/dataAccessFunctions')
from dataAccess.dataAccessFunctions import *

app = Flask(__name__)

# ------------------ error handling ---------------
class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

# ------------------ Routes ------------------
@app.route('/hello', methods = ['GET']) 
def say_hello():
    return jsonify({'message': 'Howdy!'})

@app.route('/sunspots', methods=['GET']) # route creation decorator
def get_data():

    start = request.args.get('start')
    end = request.args.get('end')
    limit = request.args.get('limit')
    offset = request.args.get('offset')

    # test if inputs are integers
    try:
        if start != None:
            start = int(start)
        if end != None:
            end = int(end)
        if limit != None:
            limit = int(limit)
        if offset != None:
            offset = int(offset)
    except:
        raise InvalidUsage('Error! Start, end, limit, and offset, if provided, must be integers.', status_code=400)
    
    # make sure either start/end and limit/offset are not provided together 
    if ('start' in request.args or 'end' in request.args) and ('limit' in request.args or 'offset' in request.args):
        raise InvalidUsage('Error! limit and/or offset cannot be combined with start and/or end', status_code=400)
    
    if 'start' in request.args or 'end' in request.args:
        if start == None and end <= 2014:
            data = listData(end=end)
            return jsonify(data)
        elif end == None and start >= 1700:
            data = listData(start=start)
            return jsonify(data)
        elif start < 1700 or end > 2014 or end < start:
            raise InvalidUsage('Error! Valid years are between 1700 and 2014. Make sure start < end.', status_code=400)
        else:
            return jsonify(listData(start=start, end=end))

    if 'limit' in request.args or 'offset' in request.args:
        if limit == None and offset < 315:
            data = sliceData(offset=offset)
            return jsonify(data)
        elif offset == None and limit < 315:
            data = sliceData(limit=limit)
            return jsonify(data)
        elif offset >= 315 or limit >= 315:
            raise InvalidUsage('Invalid values entered for offset or limit.', status_code=400)
        else:
            return jsonify(sliceData(offset=offset, limit=limit))

    return jsonify(getData())
        

@app.route('/sunspots/id/<id_num>', methods=['GET'])
def get_id(id_num):
    data = getData()
    try:
        id_num = int(id_num)
        if id_num >= 0 and id_num <= 314:
            return jsonify(data[id_num])
        else:
            raise InvalidUsage('Value out of range. Enter an id number between 0 and 314.', status_code=400)
    except:
        raise InvalidUsage('Value out of range. Enter an id number between 0 and 314.', status_code=400)

@app.route('/sunspots/year/<value>', methods=['GET'])
def get_year(value):
    try:
        value = int(value)
        if value >= 1700 and value <= 2014:
            data = listData(start=value, end=value)
            return jsonify(data)
        else:
            raise InvalidUsage('Value out of range. Enter a year between 1700 and 2014.', status_code=400)
    except:
        raise InvalidUsage('Error! Enter a year between 1700 and 2014.', status_code=400)

def validate_job():
    ''' Check a job request is valid: start and end must be supplied and be integers. '''
    pass

@app.route('/jobs', methods=['GET', 'POST'])
def jobs_api():
    ''' List all jobs and create new jobs '''
    if request.method == 'POST':
        try:
            job = request.get_json(force=True)
        except Exception as e:
            raise InvalidUsage('Invalid JSON: {}.'.format(e), status_code=400)
        return jsonify(jobs.add_job(job['start'], job['end']))

    elif request.method == 'GET':
        return jsonify(jobs.get_all_jobs())

@app.route('/jobs/<jid>', methods=['GET', 'DELETE'])
def job(jid):
    ''' List and delete job by job id '''
    if request.method == 'GET':
        return jsonify(jobs.get_job_by_jid(jid))
    else:
        jobs.delete_by_jid(jid)
        return jsonify({'status': 'Success', 'message': 'Job {} deleted.'.format(jid) })
    
@app.route('/jobs/<jid>/plot', methods=['GET'])
def job_plot(jid):
    ''' Returns the plot associated with the job as a binary png file. '''
    plot = jobs.get_job_plot(jid)
    return send_file(io.BytesIO(plot[0]), mimetype='image/png', as_attachment=True, \
                attachment_filename='{}.png'.format(jid))
                     
@app.route('/jobs/<jid>/mean', methods=['GET'])
def job_mean(jid):
    '''Returns the average of the sunspots in the data requested.'''
    mean = jobs.get_job_mean(jid)
    return jsonify({'status': 'Success', 'Mean': mean})

@app.route('/jobs/<jid>/stdev', methods=['GET'])
def job_stdev(jid):
    '''Returns the standard deviation of the sunspots data requested.'''
    stdev = jobs.get_job_std(jid)
    return jsonify({'status': 'Success', 'Standard Deviation': stdev})

@app.route('/jobs/<jid>/analysis', methods=['GET'])
def job_analysis(jid):
    '''Returns several characteristics of the analyzed data points.'''
    maxi, mini, mean, stdev = jobs.get_job_analysis(jid)
    return jsonify({'status': 'Success', 'Mean': mean, 'Maximum': maxi, 'Minimum': mini, 'Standard Deviation': stdev})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
