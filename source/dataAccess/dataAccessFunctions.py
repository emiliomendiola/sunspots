# functions used to access csv data

def getData():
    path_to_file = "/source/dataAccess/sunspots.csv"
    #path_to_file = '/home/ubuntu/sunspots/source/dataAccess/sunspots.csv' #local path
    count = 0
    data = []
    with open(path_to_file, 'r') as ifile:
        for line in ifile:
            lineData = line.strip('\n').split(',')
            data.append({'id':count, 'year':int(lineData[0]), 'sunspots':int(lineData[1])})
            count += 1
    return data

# Need to getData to correctly declare optional start/end parameters in listData
data = getData()

def listData(start=data[0]['year'], end=data[-1]['year']):
    data = getData()
    first = start - data[0]['year']
    last = end - data[0]['year'] + 1
    return data[first:last]

def sliceData(limit='none', offset=0):
    data = getData()
    first = offset
    if limit == 'none': limit = len(data)
    last = first+limit
    return data[first:last]

