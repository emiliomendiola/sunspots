FROM python:3.6

ADD source /source

RUN pip install -r source/requirements.txt

WORKDIR /source

CMD ["python3","api.py"]